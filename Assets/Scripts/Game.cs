﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CameraFacingDirection
{
    FRONT,
    RIGHT,
    BACK,
    LEFT
}
public class Game : MonoBehaviour
{
    public PlayerMovement playerMove;

    #region direction    
    CameraFacingDirection _currentDirection;
    public CameraFacingDirection CameraDirection
    {
        get
        {
            return _currentDirection;
        }
        private set
        {
            _currentDirection = value;
            MovePlayerToClosestCubeToCamera(_currentDirection);
            ActivateInvisicubes(_currentDirection);
        }
    }
    void ActivateInvisicubes(CameraFacingDirection direction)
    {
        switch (direction)
        {
            case CameraFacingDirection.FRONT:
            case CameraFacingDirection.BACK:
                InvisiRoots[0].SetActive(true);
                InvisiRoots[1].SetActive(false);
                InvisiRoots[0].transform.position = new Vector3(0f, 0f, playerMove.transform.position.z);
                break;
            case CameraFacingDirection.RIGHT:
            case CameraFacingDirection.LEFT:
                InvisiRoots[0].SetActive(false);
                InvisiRoots[1].SetActive(true);
                InvisiRoots[1].transform.position = new Vector3(playerMove.transform.position.x, 0f, 0f);
                break;
        }
    }
    public float Angle
    {
        get
        {
            return (int)_currentDirection * (-90f); 
        }
    }
    void RotateRight()
    {
        int newDirection = (int)(_currentDirection);
        if (++newDirection > 3) newDirection = 0;
        CameraDirection = (CameraFacingDirection)(newDirection);
    }
    void RotateLeft()
    {
        int newDirection = (int)(_currentDirection);
        if (--newDirection < 0) newDirection = 3;
        CameraDirection = (CameraFacingDirection)(newDirection);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
            RotateRight();
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            RotateLeft();
    }
    #endregion

    #region invisicubes
    public GameObject InvisiCube; //prefab
    public Transform Platforms; //all platforms in game
    public GameObject[] InvisiRoots;
    private void Start()
    {
        //Create all invisi cubes
        CreateInvisiCubes(CameraFacingDirection.FRONT, InvisiRoots[(int)CameraFacingDirection.FRONT].transform);
        CreateInvisiCubes(CameraFacingDirection.RIGHT, InvisiRoots[(int)CameraFacingDirection.RIGHT].transform);

        InvisiRoots[0].SetActive(true);
        InvisiRoots[1].SetActive(false);

        CreateClosestPointsToCamera();

        CameraDirection = CameraFacingDirection.FRONT;
    }
    void CreateInvisiCubes(CameraFacingDirection direction, Transform root)
    {
        HashSet<Vector2> created = new HashSet<Vector2>();
        foreach (Transform platformCube in Platforms)
        {
            Vector2 pos2d = GetInvisiPositionIn2d(platformCube.position, direction);
            if (created.Contains(pos2d)) continue;
            created.Add(pos2d);
            GameObject invisiCube = Instantiate(InvisiCube, root);
            InvisiCube.transform.position = GetInvisiPosition3d(platformCube.position, direction, 0f);
        }
    }
    Vector2 GetInvisiPositionIn2d(Vector3 position, CameraFacingDirection direction)
    {
        switch (direction)
        {
            case CameraFacingDirection.FRONT:
            case CameraFacingDirection.BACK:
                return new Vector2(position.x, position.y);
            case CameraFacingDirection.RIGHT:
            case CameraFacingDirection.LEFT:
                return new Vector2(position.z, position.y);
            default:
                return Vector2.zero;
        }
    }
    Vector3 GetInvisiPosition3d(Vector3 position, CameraFacingDirection direction, float cameraAxis)
    {
        switch (direction)
        {
            case CameraFacingDirection.FRONT:
            case CameraFacingDirection.BACK:
                return new Vector3(position.x, position.y, cameraAxis);
            case CameraFacingDirection.RIGHT:
            case CameraFacingDirection.LEFT:
                return new Vector3(cameraAxis, position.y, position.z);
            default:
                return Vector3.zero;
        }
    }
    #endregion

    #region closest_points
    Dictionary<Vector2, float>[] _closestPoints = new Dictionary<Vector2, float>[4];

    void CreateClosestPointsToCamera()
    {
        for (int i = 0; i < 4; i++)
            CreateClosestPointsToCamera((CameraFacingDirection)i);
    }
    void CreateClosestPointsToCamera(CameraFacingDirection direction)
    {
        int index = (int)direction;
        _closestPoints[index] = new Dictionary<Vector2, float>();

        foreach (Transform platformCube in Platforms)
        {
            Vector2 pos2d = GetInvisiPositionIn2d(platformCube.position, direction);
            float cameraAxis = ((int)direction % 2 == 0) ? platformCube.position.z : platformCube.position.x;

            if (!_closestPoints[index].ContainsKey(pos2d))
            {
                _closestPoints[index].Add(pos2d, cameraAxis);
            }
            else
            {
                switch (direction)
                {
                    case CameraFacingDirection.FRONT:
                        if (_closestPoints[index][pos2d] < cameraAxis) _closestPoints[index][pos2d] = cameraAxis;
                        break;
                    case CameraFacingDirection.RIGHT:
                        if (_closestPoints[index][pos2d] > cameraAxis) _closestPoints[index][pos2d] = cameraAxis;
                        break;
                    case CameraFacingDirection.BACK:
                        if (_closestPoints[index][pos2d] > cameraAxis) _closestPoints[index][pos2d] = cameraAxis;
                        break;
                    case CameraFacingDirection.LEFT:
                        if (_closestPoints[index][pos2d] < cameraAxis) _closestPoints[index][pos2d] = cameraAxis;
                        break;
                }
                
            }
        }
    }

    void MovePlayerToClosestCubeToCamera(CameraFacingDirection direction)
    {
        float closestBelowDistance = -1f;
        Vector3 closestBelowPosition = Vector3.zero;
        foreach (Transform platformCube in Platforms)
            if (IsBelowPlayer(playerMove.transform.position, platformCube.transform.position))
                if (closestBelowDistance == -1 || playerMove.transform.position.y - platformCube.transform.position.y < closestBelowDistance)
                    closestBelowPosition = platformCube.transform.position;
        if (closestBelowDistance == -1f) return;

        Vector2 pos2d = GetInvisiPositionIn2d(closestBelowPosition, direction);
        playerMove.transform.position = GetInvisiPosition3d(playerMove.transform.position, direction, _closestPoints[(int)direction][pos2d]);
    }
    bool IsBelowPlayer(Vector3 playersPosition, Vector3 cubePosition)
    {
        return playersPosition.x == cubePosition.x && playersPosition.z == cubePosition.z && playersPosition.y > cubePosition.y;
    }
    #endregion

    #region singletone
    public static Game Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion
}
